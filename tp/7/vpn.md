# IV. VPN

Dans cette partie, on va setup un serveur VPN.

## Sommaire

- [IV. VPN](#iv-vpn)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [1. Explications](#1-explications)
  - [2. Schémas](#2-schémas)
  - [3. On y va ou bien](#3-on-y-va-ou-bien)
    - [A. Setup serveur](#a-setup-serveur)
    - [B. Setup client](#b-setup-client)

![VPN hehe](./img/vpn.jpg)

## 0. Setup

| Name            | LAN1 `10.7.1.0/24` |
| --------------- | ------------------ |
| `router.tp7.b1` | `10.7.1.254`       |
| `john.tp7.b1`   | `10.7.1.11`        |
| `vpn.tp7.b1`    | `10.7.1.101`       |

## 1. Explications

**Un serveur VPN permet simplement à des clients distants d'accéder au même réseau LAN.**

Faire comme si oin était physiquement au même endroit : être dans un LAN. Mais en étant à distance : chacun chez nous avec internet à dispo. Ca permet de résoudre plein de problématiques comme :

- se connecter au même VPN qu'un pote pour faire une LAN et geeker comme si on était physiquement côte à côte
- se connecter de façon sécurisée au réseau d'une entreprise à distance pour gérer les serveurs quand y'a le COVID, ou quand c'est en Uruguay (psk c loin)
- pseudo-anonymiser son identité sur Internet en utilisant l'adresse du serveur VPN comme adresse source du trafic
- d'autres machins

> *Pour des raisons évidentes de contraintes matérielles, on va se cantonner à quelques VMs dans un environnement virtuel pour ce TP.*

Ce serait exactement et rigoureusement la même chose si par exemple vous :

- louez un petit serveur en ligne pour avoir votre propre VPN comme vous l'imaginez
- hébergez un serveur VPN chez vous, pour avoir un accès distant au réseau de chez vous

Dans certains setups, on aime bien monter le serveur VPN sur le routeur du réseau si c'est possible. Ici, pour bien tout séparer dans vos esprits, on va faire une machine virtuelle séparée, qui servira de serveur VPN.

## 2. Schémas

Quelques schémas pour clarifier autant que possible ce que vous faites dans cette partie.

➜ **Sans VPN actuellement on a ça**

![Initial setup](./img/vpn_initial.svg)

> *Mes images c'est format SVG, c'est à dire qu'on peut les agrandir à l'infini et ce sera jamais flou. Si là c'est moche (outre les goûts de chiottes pour les couleurs) c'est que c'est trop petit. Ouvrez-les en grand pour mieux voir si nécessaire.*

- `john` contacte directement le routeur pour accéder à internet
- l'identité de `john` quand il accède à internet
  - au sein du LAN, le routeur sait que `john` accède à internet
  - au sein du WAN, `john` a pour identité l'IP publique du routeur

---

➜ **Avec ce que vous faites dans le TP vous aurez ça**

![Lab setup](./img/vpn_lab.svg)

- `john` contacte le serveur VPN pour accéder à internet
- l'identité de `john` quand il accède à internet
  - au sein du LAN
    - le routeur
      - ne pas sait pas que `john` accède à internet
      - pense que le serveur VPN accède à internet
    - le serveur VPN
      - sait que `john` accède à internet et voit son trafic
  - au sein du WAN, `john` a pour identité l'IP publique du routeur

---

➜ **Quand vous utilisez un VPN public ça ressemble à ça**

![Real setup](./img/vpn_real.svg)

- on change juste la ligne pointillé rose en fait : comment vous accédez au serveur VPN
- `john` contacte le serveur VPN pour accéder à internet
- l'identité de `john` quand il accède à internet
  - au sein du LAN
    - le routeur
      - sait que `john` accède à internet, mais ne peut pas voir quel trafic
  - au sein du LAN du serveur VPN
    - le serveur VPN
      - sait que `john` accède à internet et voit son trafic
  - au sein du WAN, `john` a pour identité l'IP publique du serveur VPN

## 3. On y va ou bien

### A. Setup serveur

➜ **Créez la machine `vpn.tp7.b1`** si c'est pas déjà fait et déroulez la checklist.

On va utiliser Wireguard pour monter notre serveur VPN. Il est libre, opensource, assez simple d'utilisation, performant, carrément hipster en terme de choix d'algo de chiffrement, natif sous Linux, client cross-platform (Windows étou), une commu active, il a tout pour plaire ce bogoss.

Tout est à réaliser sur `vpn.tp7.b1`.

🌞 **Installer le serveur Wireguard sur `vpn.tp7.b1`**

```bash
# on active le module noyau Wireguard
# un module noyau c'est un logiciel, mais il sera plus rapide qu'un machin classique, pour faire TRES simple
$ sudo modprobe wireguard
$ echo wireguard | sudo tee /etc/modules-load.d/wireguard.conf

# modifier le fichier suivant avec nano, je vous montre son contenu avec cat
$ sudo cat /etc/sysctl.conf
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1

# appliquer les changements
$ sudo sysctl -p

# on installe les outils liés à Wireguard
$ sudo dnf install wireguard-tools -y
```

🌞 **Générer la paire de clé du serveur sur `vpn.tp7.b1`**

```bash
# on génère une clé privée, et on la stocke dans un fichier dédié, dans le dossier /etc/wireguard
wg genkey | sudo tee /etc/wireguard/server.key

# on définit des permissions restrictives sur la clé
sudo chmod 0400 /etc/wireguard/server.key

# on génère la clé publique
sudo cat /etc/wireguard/server.key | wg pubkey | sudo tee /etc/wireguard/server.pub
```

🌞 **Générer la paire de clé du client sur `vpn.tp7.b1`**

```bash
# on crée un dossier pour héberger les clés de nos clients
$ sudo mkdir -p /etc/wireguard/clients

# on génère une clé privée, et on la stocke dans un fichier dédié, dans le dossier /etc/wireguard
$ wg genkey | sudo tee /etc/wireguard/clients/john.key

# on définit des permissions restrictives sur la clé
$ sudo chmod 0400 /etc/wireguard/clients/john.key

# on génère la clé publique
$ sudo cat /etc/wireguard/clients/john.key | wg pubkey | tee /etc/wireguard/clients/john.pub
```

🌞 **Création du fichier de config du serveur sur `vpn.tp7.b1`**

- je vous montre le contenu du fichier, utilisez `nano` de votre côté pour éditer le fichier

```bash
$ sudo cat /etc/wireguard/wg0.conf
[Interface]
Address = 10.107.1.0/24
SaveConfig = false
PostUp = firewall-cmd --zone=public --add-masquerade
PostUp = firewall-cmd --add-interface=wg0 --zone=public
PostDown = firewall-cmd --zone=public --remove-masquerade
PostDown = firewall-cmd --remove-interface=wg0 --zone=public
ListenPort = 13337
PrivateKey = <clé du fichier /etc/wireguard/server.key>

[Peer]
PublicKey = <clé du fichier /etc/wireguard/clients/john.pub>
AllowedIPs = 10.107.1.11/32
```

🌞 **Démarrez le serveur VPN sur `vpn.tp7.b1`**

```bash
# démarrer le serveur VPN
sudo systemctl start wg-quick@wg0.service`

# une nouvelle interface réseau a pop
ip a

# voir les connexions des clients
wg show
```

🌞 **Ouvrir le bon port firewall**

- on a demandé dans le fichier de conf que le serveur écoute sur le port 13337
- utilisez une commande `ss` pour identifier si c'est du TCP ou de l'UDP
- ouvrez le port correspondant dans le firewall

> **Dans un setup réel il est HORS DE QUESTION que le serveur génère des clés pour le client.** Peu importe le contexte (VPN ou autres) C'est normalement au client lui-même de générer ses clés, et de passer sa clé publique au serveur. Ici on fait comme ça pour des raisons de simplicité. Y'a aucune sécu si c'est pas toi qui génère ta clé. C'est comme si le serveur choisissait ton password.

### B. Setup client

Tout est à réaliser sur `john.tp7.b1`.

🌞 **On installe les outils Wireguard sur `john.tp7.b1`**

```bash
$ sudo dnf install wireguard-tools
```

🌞 **Supprimez votre route par défaut**

- comme ça on est sûrs que `john` n'a pu internet : il l'aura en passant par le VPN !
- prouvez que vous n'avez plus internet

🌞 **Configurer un fichier de conf sur `john.tp7.b1`**

- utilisez `nano` pour éditer le fichier, je vous montre son contenu avec `cat`

```bash
# déplacez-vous dans votre répertoire personnel
$ cd

# créez un dossier wireguard
$ mkdir wireguard

# créez le fichier de configuration (avec nano)
$ sudo cat wireguard/john.conf 
[Interface]
Address = 10.107.1.11/24
PrivateKey = <clé contenue dans /etc/wireguard/clients/john.key sur le serveur> 

[Peer]
PublicKey = <clé contenue dans /etc/wireguard/server.pub sur le serveur> 
AllowedIPs = 0.0.0.0/0
Endpoint = 10.7.1.101:13337
```

🌞 **Lancer la connexion VPN**

```bash
# déplacez-vous dans votre dossier wireguard
$ cd wireguard

# et lancez la connexion en indiquant le fichier de conf :
$ wg-quick up ./john.conf
```

🌞 **Vérifier la connexion**

- sur `john.tp7.b1`
  - `ping 1.1.1.1` et `ping ynov.com` et `traceroute 1.1.1.1`
  - `ip a`
  - `wg show`
- sur `vpn.tp7.b1`
  - `wg show`

🦈 **Capture `tp7_vpn_in.pcapng` et `tp7_vpn_out.pcapng`**

- pendant que `john` fait un `ping 1.1.1.1`
- capturez le trafic depuis le serveur VPN, interface host-only, on devrait voir :
  - du trafic UDP entre le serveur et le client
  - des ping partir depuis le serveur VPN vers 1.1.1.1
