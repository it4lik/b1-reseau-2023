# II. SSH

Dans cette section on va s'intéresser à la place du chiffrement dans l'utilisation du protocole SSH.

En particulier, on va :

- en tant que client qui se connecte, vérifier qu'on se connecte au bon serveur SSH
  - cool pour éviter les attaques man-in-the-middle par exemple
- en tant que client, utiliser une clé plutôt qu'un password pour se connecter au serveur
  - on évite l'erreur humaine
  - on évite l'échange d'un password qui est une donnée sensible à travers le réseau
  - avec une clé, à aucun moment une donnée sensible ne circule sur le réseau
- en profiter pour faire un peu de conf du serveur SSH, lié au réseau

## Sommaire

- [II. SSH](#ii-ssh)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [1. Fingerprint](#1-fingerprint)
    - [A. Explications](#a-explications)
    - [B. Manips](#b-manips)
  - [2. Conf serveur SSH](#2-conf-serveur-ssh)
  - [3. Connexion par clé](#3-connexion-par-clé)
    - [A. Explications](#a-explications-1)
    - [B. Manips](#b-manips-1)
    - [C. Changement de fingerprint](#c-changement-de-fingerprint)

![SSH keys](./img/use_ssh_key.png)

## 0. Setup

| Name            | LAN1 `10.7.1.0/24` |
| --------------- | ------------------ |
| `router.tp7.b1` | `10.7.1.254`       |
| `john.tp7.b1`   | `10.7.1.11`        |

Pas de nouvelles machines pour cette section donc.

## 1. Fingerprint

### A. Explications

💡 **Cette section explique comment vous pouvez vous assurer que vous vous connectez au bon serveur SSH.**

Qu'un vilain hacker n'est pas entre vous et le serveur SSH (*man-in-the-middle*) par exemple.

➜ **Lorsqu'on se connecte à une machine en SSH pour la première fois, apparaît un message comme celui-ci :**

```bash
The authenticity of host '10.7.1.103 (10.7.1.103)' can't be established.
ED25519 key fingerprint is SHA256:CoMGKXc0JWXwmMEiVCxxJ7SifJW18MfCLGMJKJMNO9A.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

Ce message nous informe de l'empreinte (ou *fingerprint* en anglais) du serveur : c'est une chaîne de caractère qui identifie de façon unique le serveur.  
Dans l'exemple au dessus, le serveur `10.7.1.103` me présente son empreinte : `CoMGKXc0JWXwmMEiVCxxJ7SifJW18MfCLGMJKJMNO9A`.

> *Certains serveurs SSH ont plusieurs clés pour différents contextes, et donc plusieurs empreintes. Ici, le serveur nous indique précisément qu'il nous présente le hash en `sha256` de sa clé qui utilise l'algorithme `ED25519`.*

Je peux donc choisir de faire confiance, de trust, cette empreinte et saisir `yes`. A l'inverse je peux saisir `no` si je trust pas et ainsi annuler la connexion.

➜ **Comment trust ?**

Il faut se connecter manuellement au serveur, sans SSH donc. Dans notre cas, utilisez la console de la VM directement.  
On peut alors utiliser une commande pour consulter l'empreinte du serveur directement :

> *Comme indiqué au dessus, ce qu'on va vérifier, c'est précisément l'empreinte `sha256` de sa clé en `ED25519`.*

```bash
# on regarde les clés du serveur qui existe
$ ls /etc/ssh

# on peut alors calculer l'empreinte sha256 d'une clé spécifique avec ssh-keygen -l
$ ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key
```

➜ **Le message n'apparaît plus aux connexions suivantes**

- c'est uniquement à la première connexion ce message
- après, ça s'affiche plus jamais, car votre PC a enregistré l'empreinte
- à chaque connexion suivante, il vérifie que l'empreinte est la bonne automatiquement
- le fichier qui stocke les empreintes que vous connaissez est appelé `known_hosts`

> C'est dans le nom `known_hosts` : les hôtes connus, littéralement !

`known_hosts` est un simple fichier texte qui se trouve dans votre répertoire personnel, dans un sous-dossier `.ssh` :

```bash
# Avec un OS Linux
$ ls /home/<USER>/.ssh

# Windows
$ ls C:/Users/<USER>/.ssh

# MacOS je suppose que c'est 
$ ls /Users/<USER>/.ssh
```

### B. Manips

> **Si tu t'es déjà connecté à `john` en SSH, tu n'auras pas le message de première connexion.** Il faudra donc éditer ton fichier `known_hosts`, supprimer la ligne qui concerne `john` afin d'avoir de nouveau le message du serveur qui présente son fingerprint, comme à a la première connexion.

🌞 **Effectuez une connexion SSH en vérifiant le fingerprint**

- en rendu je veux voir le message du serveur à la première connexion
  - une commande `ssh` pour se connecter vers `john`
- et je veux aussi une commande qui me montre l'empreinte du serveur
- ce doit être les mêmes évidemment normalement !

> Fais l'exo normalement : d'abord vérifie l'empreinte depuis l'interface console de la VM. Pour l'écriture du rendu, tu te co en SSH en validant l'empreinte avec "yes" puis tu tapes de nouveau la commande pour vérifier l'empreinte, et tu peux donc copier/coller le résultat dans le rendu.

## 2. Conf serveur SSH

On va faire un truc très basique, pour manipuler un peu toujours les cartes réseau, les IP, les ports, toussa : on va choisir explicitement l'IP et le port où tourne notre serveur SSH sur `router`.

La configuration du serveur SSH se fait dans le fichier `/etc/ssh/sshd_config`, utilisez `nano` pour le modifier par exemple.

Il faut être admin pour modifier le fichier, il faudra donc préfixer votre commande avec `sudo`.

🌞 **Consulter l'état actuel**

- vérifiez que le serveur SSH tourne actuellement sur le port 22/tcp
- vérifiez que le serveur SSH est disponible actuellement sur TOUTES les IPs de la machine
- ça se fait en une seule commande `ss` avec les bonnes options (appelez-moi si besoin d'aide)

> "Toutes les IPs", y'aura écrit `0.0.0.0:22`. `0.0.0.0` ça veut dire "toute les adresses IP de la machine". Y'aura ptet aussi `[::]:22`, c'est en IPv6 on s'en fout pour le moment. Ici donc, pour votre routeur, **"toutes les IPs" ça veut dire les deux cartes réseau.** On va limiter l'accès SSH en ne permettant la connexion que depuis le réseau host-only. Dans un contexte réel, c'est un peu comme si vous autorisiez la connexion à votre box uniquement depuis votre LAN, et pas depuis internet. Ca paraît pas déconnant si ? Sauf si tu veux que tout internet se connecte à ton routeur !

🌞 **Modifier la configuration du serveur SSH**

- sur `router.tp7.b1` uniquement
- éditer le fichier de configuration du serveur SSH pour spécifier que :
  - le serveur doit écouter sur l'adresse IP `10.7.1.254`
  - le serveur doit écouter sur le port de votre choix entre 20000 et 30000

> N'oubliez pas de redémarrer le serveur SSH pour que les changements prennent effet : `sudo systemctl restart sshd`.

🌞 **Prouvez que le changement a pris effet**

- toujours avec la même commande `ss` vous devriez voir que :
  - le serveur SSH écoute désormais sur `10.7.1.254` uniquement
  - le serveur SSH écoute désormais sur le port choisi

🌞 **N'oubliez pas d'ouvrir ce nouveau port dans le firewall**

- comme d'hab, voir mémo pour voir comment ouvrir un port
- sans ouverture du port, le firewall refusera que vous vous vonnectiez sur le nouveau port

🌞 **Effectuer une connexion SSH sur le nouveau port**

- depuis votre PC, connctez-vous en SSH au serveur
- d'habitude, c'est le port standard 22/tcp, bah la commande SSH essaie automatiquement de se co au port 22/tcp
- vu qu'on est passés du port standard (22/tcp) à un port de merde, il faut spécifier explicitement à quel port on se connecte

```
$ ssh toto@router.tp7.b1 -p <PORT>
```

## 3. Connexion par clé

### A. Explications

💡 **Cette section explique comment vous pouvez remplacer l'utilisation d'un password (faible en terme de sécurité) par une clé (fort en terme de sécurité).**

Quand on se connecte à une machine (genre toi quand t'ouvres ton windows le matin, ou quand tu te co à une machine en SSH, c'est la même chose), l'OS nous demande de saisir un password pour ouvrir une session avec un utilisateur de la machine. Rien de nouveau sous le soleil mao.

Il se trouve qu'en terme de sécurité, en particulier pour des connexions à distance comme avec SSH, **bah c'est claqué l'utilisation d'un password**.  
On fait circuler le password sur le réseau, alors qu'il est hyper sensible, c'est nul ça.  
Puis tu dois t'en souvenir, ou le stocker dans une application. C'est CLAQUE.

**Pour augmenter le niveau de sécurité de la connexion, on préfère remplacer l'utilisation d'un password par l'utilisation d'une paire de clé.**

Nous on est juste des utilisateurs de ça, personne vous demande de comprendre les maths derrière le bail. On tape 3 commandes, ça marche, on saisit plus jamais de password et on a un meilleur niveau de sécurité. La vie est belle.

L'idée c'est :

- une seule fois, vous générez une paire de clés sur VOTRE PC
  - votre clé privée, vous l'ouvrez jamais, ne la regardez jamais, ne la partagez jamais, jamais quoi
  - la clé publique on s'en balec, tu peux la donner partout, donc c'est celle-ci qu'on partage
- vous déposez la clé publique sur la machine à laquelle vous souhaitez vous connecter
  - ou sur les 10000 machines auxquelles vous voulez vous connecter
- vous pouvez désormais vous connecter sans saisir de password avec un meilleur niveau de sécu

Nice and easy. 

### B. Manips

Let's go :

🌞 **Générer une paire de clés**

- **SUR VOTRE PC**, pas dans une VM, ce sera votre paire de clé, qui remplace votre password
- avec la commande :

> **Si vous avez déjà une paire clé**, n'en regénérez pas forcément une, m'enfou. Continuez avec celle que vous avez déjà.

```bash
$ ssh-keygen -t rsa -b 4096
# ne changez pas le chemin qui vous est proposé pour la clé, ça vous évitera d'avoir à le saisir tout le temps
```

➜ Consultez l'existence des clés si besoin

- je veux dire c'est juste deux fichiers
- en explorateur de fichiers, graphiquement, tu peux voir tes deux clés dans le dossier `.ssh` de ton répertoire personnel

🌞 **Déposer la clé publique sur une VM**

- **depuis votre PC toujours**, il existe une commande pour faire ça
- comme ça on se fait pas chier à transporter le fichier à la main, et à faire gaffe à le mettre au bon endroit étou
- déposez la clé sur `john`

> ***Sous Windows**, vous devrez utiliser Git Bash pour cette étape.*

```bash
# en supposant que vous utilisez la commande suivante pour vous connecter à john :
ssh toto@john.tp7.b1

# vous pouvez déposer votre clé publique avec
ssh-copy-id toto@john.tp7.b1
```

🌞 **Connectez-vous en SSH à la machine**

- comme vous l'a normalement suggéré la commande précédente, connectez-vous à la machine
- connecez-vous donc à `john`
- **normalement, aucun password ne vous sera demandé**

### C. Changement de fingerprint

Si un jour, quand tu te connectes à un serveur auquel tu t'es déjà connecté avant (genre sur la même adresse IP) mais que le fingerprint a changé, alors la connexion va échouer.

En effet comme on a dit, grâce au fichier `known_hosts` votre PC enregistre les fingerprint que vous acceptez et compare automatiquement à chaque fois.

Vous mangez donc un beau message d'avertissement et la connexion échoue si ça arrive.

On va provoquer cette situation, pour que vous expérimentiez le truc. Ce qu'on va faire dans cette partie :

- supprimer les clés sur le serveur SSH
- regénérez des nouvelles clés (les empreintes seront donc différentes)
- faire une connexion SSH et manger un beau message d'avertissement avec une connexion échouée
- remédier à la situation

🌞 **Supprimer les clés sur la machine `router.tp7.b1`**

- utilisez la commande `rm` pour effectuer une suppression
- supprimez tous les fichiers qui commencent par `ssh_host_` dans le dossier `/etc/ssh/` : ce sont les clés du serveur

🌞 **Regénérez les clés sur la machine `router.tp7.b1`**

- y'a une commande qui permet de faire ça vitefé : `sudo ssh-keygen -A` et ça regénère tout
- n'oubliez pas de redémarrer le service SSH après ça : `sudo systemctl restart sshd`

🌞 **Tentez une nouvelle connexion au serveur**

- déconnectez-vous de votre session SSH actuelle
- effectuez une nouvelle connexion
- interprétez le message pour remédier à la situation et pouvoir vous connecter
- n'hésitez pas à m'appeler si vous voulez de l'aide pour interpréter ce message
