# TP7 : Do u secure

Dans ce TP on va aborder plusieurs cas d'application de la cryptographie en informatique.

On va donc voir ici 3 cas d'application :

- SSH
  - le serveur prouve son identité aux gens qui se connectent
  - le client utilise une paire de clé plutôt qu'un apssword pour se co
- HTTPS
  - le serveur prouve son identité avec un certificat (qui contient sa clé publique)
  - le client et le serveur établissent une connexion chiffrée avec chacun une paire de clés, dans le but de délivrer du trafic HTTP
- bonus : VPN
  - le serveur prouve son identité au client
  - le client prouve son identité au serveur
  - le client et le serveur établissent une connexion chiffrée avec chacun une paire de clés, dans le but que le client accès à un LAN à distance

## Sommaire

- [TP7 : Do u secure](#tp7--do-u-secure)
  - [Sommaire](#sommaire)
- [0. Setup](#0-setup)
- [I. Setup LAN](#i-setup-lan)
- [II. SSH](#ii-ssh)
- [III. Web](#iii-web)
- [IV. Bonus : VPN](#iv-bonus--vpn)

# 0. Setup

➜ Pour chaque VM, vous déroulerez la checklist suivante :

- [x] Créer la machine (avec une carte host-only)
- [x] Définir une IP statique à la VM
- [x] Donner un hostname à la machine
- [x] Utiliser SSH pour administrer la machine
- [x] Remplir votre fichier `hosts`, celui de votre PC, pour accéder au VM avec un nom
- [x] Dès que le routeur est en place, n'oubliez pas d'ajouter une route par défaut aux autres VM pour qu'elles aient internet

# I. Setup LAN

| Name            | LAN1 `10.7.1.0/24` |
| --------------- | ------------------ |
| `router.tp7.b1` | `10.7.1.254`       |
| `john.tp7.b1`   | `10.7.1.11`        |

🖥️ **Machine `router.tp7.b1`**

- ajoutez lui aussi une carte NAT en plus de la carte host-only (privé hôte en français) pour qu'il ait un accès internet
- toutes les autres VMs du TP devront utiliser ce routeur pour accéder à internet
- n'oubliez pas d'activer le routage vers internet sur cette machine :

```
$ sudo firewall-cmd --add-masquerade --permanent
$ sudo firewall-cmd --reload
```

🖥️ **Machine `john.tp7.b1`**

- une machine qui servira de client au sein du réseau pour effectuer des tests
- suivez-bien la checklist !
- testez tout de suite avec `john` que votre routeur fonctionne et que vous avez un accès internet

# II. SSH

[Clic ici pour accéder la partie II sur SSH.](./ssh.md)

# III. Web

[Clic ici pour accéder la partie III sur le serveur Web avec HTTPS.](./web.md)

# IV. Bonus : VPN

[Clic ici pour accéder la partie IV bonus sur le serveur VPN.](./vpn.md)
