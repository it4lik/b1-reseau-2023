# III. Web sécurisé

On va mettre en place un petit serveur HTTPS dans cette section. On va encore héberger un MEOW, MAIS de façon sécurisée.

- [III. Web sécurisé](#iii-web-sécurisé)
  - [0. Setup](#0-setup)
  - [1. Setup HTTPS](#1-setup-https)

![HTTPS](./img/do_you_even.jpg)

## 0. Setup

| Name            | LAN1 `10.7.1.0/24` |
| --------------- | ------------------ |
| `router.tp7.b1` | `10.7.1.254`       |
| `john.tp7.b1`   | `10.7.1.11`        |
| `web.tp7.b1`    | `10.7.1.12`        |

➜ **Créez la machine `web.tp7.b1`** et déroulez la checklist.

➜ **Référez-vous au TP5 et déroulez la partie II.3. Serveur Web sur `web.tp7.b1`**

- ne passez à la suite qu'une fois que votre serveur web est disponible en HTTP
- faites les tests depuis `john` avec la commande `curl` ou depuis votre PC avec un navigateur ou la commande `curl`

🌞 **Montrer sur quel port est disponible le serveur web**

- avec une commande `ss` sur la machine `web.tp7.b1`

## 1. Setup HTTPS

Pour avoir un beau cadenas vert à côté de la barre d'URL dans le navigateur, il faut avoir une IP publique, un nom de domaine public etc, et this costs money sir.

Dans notre lab ici, on va fabriquer un certificat "auto-signé". La connexion sera chiffrée, mais cadenas rouge. Sécurisé si vous pouvez attester vous-mêmes de la validité du certificat.

![Self signed](./img/secure.png)

🌞 **Générer une clé et un certificat sur `web.tp7.b1`**

```bash
# génération de la clé et du certificat
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt

# on déplace la clé dans un répertoire standard pour les clés
# et on la renomme au passage
$ sudo mv server.key /etc/pki/tls/private/web.tp7.b1.key

# pareil pour le cert
$ sudo mv server.crt /etc/pki/tls/certs/web.tp7.b1.crt

# on définit des permissions restrictives sur les deux fichiers
$ sudo chown nginx:nginx /etc/pki/tls/private/web.tp7.b1.key
$ sudo chown nginx:nginx /etc/pki/tls/certs/web.tp7.b1.crt
$ sudo chmod 0400 /etc/pki/tls/private/web.tp7.b1.key
$ sudo chmod 0444 /etc/pki/tls/certs/web.tp7.b1.crt
```

🌞 **Modification de la conf de NGINX**

- je vous montre le fichier avec `cat`, éditez-le avec `nano` de votre côté

```bash
[it4@web ~]$ sudo cat /etc/nginx/conf.d/site_web_nul.conf
server {
    # on change la ligne listen
    listen 10.7.1.12:443 ssl;

    # et on ajoute deux nouvelles lignes
    ssl_certificate /etc/pki/tls/certs/web.tp7.b1.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp7.b1.key;

    server_name www.site_web_nul.b1;
    root /var/www/site_web_nul;
}
```

🌞 **Conf firewall**

- ouvrez le port 443/tcp dans le firewall de `web.tp7.b1`

🌞 **Redémarrez NGINX**

- avec un `sudo systemctl restart nginx`

🌞 **Prouvez que NGINX écoute sur le port 443/tcp**

- avec une commande `ss`

🌞 **Visitez le site web en https**

- avec un `curl` depuis `john.tp7.b1`
  -  il faudra ajouter l'option `-k` pour que ça marche : `curl -k https://10.7.1.12`
- et testez avec votre navigateur aussi, histoire de !
