# B1 Réseau 2023

Ici vous trouverez tous les supports de cours, TPs et autres ressources liées au cours.

## [Cours](./cours/README.md)

- [Adresses MAC](./cours/mac_address/README.md)
- [Adresses IP](./cours/ip_address/README.md)
- [ARP](./cours/arp/README.md)
- [Routage](./cours/routage/README.md)
- [DHCP](./cours/dhcp/README.md)
- [TCP et UDP](./cours/tcp_udp/README.md)

## [TPs](./tp/README.md)

- [TP1 : Premier pas réseau](./tp/1/README.md)
- [TP2 : Ethernet, IP, et ARP](./tp/2/README.md)
- [TP3 : On va router des trucs](./tp/3/README.md)
- [TP4 : DHCP](./tp/4/README.md)
- [TP5 : TCP, UDP et services réseau](./tp/5/README.md)
- [TP6 : Un LAN maîtrisé meo ?](./tp/6/README.md)
- [TP7 : Do u secure](./tp/7/README.md)

### [Mémo](./cours/memo/README.md)

- [Rocky network](./cours/memo/rocky_network.md)
- [Install de VM Rocky](./cours/memo/install_vm.md)
